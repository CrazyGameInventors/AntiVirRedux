﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SceneController : MonoBehaviour
{
    [System.Serializable]
    public class UI
    {
        public RectTransform HUD;
        public RectTransform switchUnitInterface;
        public RectTransform pauseMenu;
    }

    public float levelTime = 60;
    public Slider timeSlider;
    public Text timeText;
    public UI uI;

    private bool paused;
    private float levelTimeStart;

    // Use this for initialization
    void Start()
    {
        paused = false;
        levelTimeStart = levelTime;
    }

    // Update is called once per frame
    void Update()
    {
        UpdateTimer();
        CheckPauseMenu();
    }

    void UpdateTimer()
    {
        levelTime -= Time.deltaTime;
        if (levelTime <= 0)
        {
            levelTime = 0;
            // End level because of time over
            EndScene(false);
        }

        timeSlider.value = 1 - (levelTime / levelTimeStart);
        int temp = (int)(timeSlider.value * 100);
        timeText.text = temp + "%";
    }

    void EndScene(bool win)
    {

    }

    void CheckPauseMenu()
    {
        if (Input.GetButtonDown(InputManager.CANCEL))
        {
            uI.HUD.gameObject.SetActive(paused);
            uI.pauseMenu.gameObject.SetActive(!paused);
            paused = !paused;
        }
    }
}
