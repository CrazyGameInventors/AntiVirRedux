﻿public class InputManager
{
    public static readonly string HORIZONTAL = "Horizontal";
    public static readonly string VERTICAL = "Vertical";
    public static readonly string FIRE_1 = "Fire1";
    public static readonly string FIRE_2 = "Fire2";
    public static readonly string FIRE_3 = "Fire3";
    public static readonly string JUMP = "Jump";
    public static readonly string MOUSE_X = "Mouse X";
    public static readonly string MOUSE_Y = "Mouse Y";
    public static readonly string MOUSE_SCROLLWHEEL = "Mouse ScrollWheel";
    public static readonly string SUBMIT = "Submit";
    public static readonly string CANCEL = "Cancel";
}

public class TagManager
{
    public static readonly string RESPAWN = "Respawn";
    public static readonly string FINISH = "Finish";
    public static readonly string EDITOR_ONLY = "EditorOnly";
    public static readonly string MAIN_CAMERA = "MainCamera";
    public static readonly string PLAYER = "Player";
    public static readonly string GAME_CONTROLLER = "GameController";
    public static readonly string JUMP_RESET = "JumpReset";
    public static readonly string SWITCH_UNIT = "SwitchUnit";
}

public class Layer
{

}