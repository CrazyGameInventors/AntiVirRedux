﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Switchable : MonoBehaviour
{
    public string switchText;

    public abstract void Switch();
}
