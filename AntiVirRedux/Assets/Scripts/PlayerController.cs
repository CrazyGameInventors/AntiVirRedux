﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float speed = 1.0f;
    public float jumpForce = 10.0f;

    private bool jumpable = true;
    private bool switchUnitInteractable = false;
    private Rigidbody playerRigidbody;
    private SwitchUnitController closestSwitchUnit;

    // Use this for initialization
    void Start()
    {
        playerRigidbody = gameObject.GetComponent<Rigidbody>();
        closestSwitchUnit = null;
    }

    // Update is called once per frame
    void Update()
    {
        MoveCharackter();
    }

    private void OnTriggerEnter(Collider other)
    {
        //Reset jump ability
        if (other.gameObject.tag == TagManager.JUMP_RESET)
        {
            // Raycast downwards to check if standing on a plattform
            jumpable = true;
        }

        // Able to interact with the switch unit
        if (other.gameObject.tag == TagManager.SWITCH_UNIT)
        {
            if (closestSwitchUnit == null)
            {
                closestSwitchUnit = other.gameObject.GetComponent<SwitchUnitController>();
            }
            else if (Vector3.Distance(gameObject.transform.position, other.gameObject.transform.position) < Vector3.Distance(gameObject.transform.position, closestSwitchUnit.gameObject.transform.position))
            {
                closestSwitchUnit = other.gameObject.GetComponent<SwitchUnitController>();
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        // Unable to interact with switch unit
        if (other.gameObject.tag == TagManager.SWITCH_UNIT)
        {

        }
    }

    void MoveCharackter()
    {
        // Move horizontal
        Vector3 movement = new Vector3(Input.GetAxisRaw(InputManager.HORIZONTAL), 0, 0);
        movement *= speed;
        movement *= Time.deltaTime;
        gameObject.transform.position += movement;

        // Jump
        if (Input.GetButtonDown(InputManager.JUMP) && jumpable)
        {
            playerRigidbody.AddForce(Vector3.up * jumpForce);
            jumpable = false;
        }
    }
}
